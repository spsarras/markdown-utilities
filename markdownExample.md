# keras-yolo3

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](LICENSE)

## Introduction

A Keras implementation of YOLOv3 (Tensorflow backend) inspired by [allanzelener/YAD2K](https://github.com/allanzelener/YAD2K).

<change> asd sdfsd dsfds </change>

<addition> asd sdfsd dsfds </addition>

<deletion> asd sdfsd dsfds </deletion>

hello "asd" hi again

<randomTag> asd sdfsd dsfds </randomTag>

This [@reference] is simple text *italic* **bold**

See figure \ref{fig_tag}

asd [@reference, @reference] asd


![Link Name](Path){ width=100% height=100% }


| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned |  1600 |
| col 2 is |    centered   |    12 |
| col 3 is | right-aligned |     1 |
Table: Name

-----------------------------------------
Tables      Are         Cool
----------- ----------- -----------------
Col 1       Row with    Third with
                        multiline       

Col 2       Row with    without multiline
-----------------------------------------
Table: Caption

---


> asd asd

* asd
* asd

- asd
- asd
    + asd
        * asd
            - asd

+ asd
+ asd

*italic*
text *italic* text
**bold**

<!-- text **bold** text -->

<!-- comment -->

```cs
a = 10
for i in range(10):
	pass

```

	code block

[Link Name](Path)

![Link Name](Path){ width=100% height=100% }

[Non_Visible_Tag]: Image_Path "Text"{ width=100% height=100% }

![Text \label{Non_Visible_Tag}][Non_Visible_Tag]

